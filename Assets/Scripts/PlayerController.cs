﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour 
{

    Touch touch;
    Vector3 startPos;
    Vector3 endPos;

	void Update()
    {
        SwipeControl();
        KeyBoardControl();
    }

    void SwipeControl()
    {
        if(Input.touchCount > 0)
        {
            touch = Input.touches[0];

            if(touch.phase == TouchPhase.Began)
            {
                startPos = touch.position;
            }

            if(touch.phase == TouchPhase.Ended)
            {
                endPos = touch.position;

                MovePlayer();
            }
        }
    }

    void KeyBoardControl()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow) && transform.position.x > -3f)
        {
            //Left
            transform.position += new Vector3(-1.5f, 0f, 0f);
        }
        if (Input.GetKeyDown(KeyCode.RightArrow) && transform.position.x < 3f)
        {
            //Right
            transform.position += new Vector3(1.5f, 0f, 0f);
        }
    }

    void MovePlayer()
    {
        if ((startPos.x > endPos.x) && (Mathf.Abs(startPos.x - endPos.x) > 25f) && transform.position.x > -3f)
        {
            //Left
            transform.position += new Vector3(-2f, 0f, 0f);
        }
        if ((startPos.x < endPos.x) && (Mathf.Abs(startPos.x - endPos.x) > 25f) && transform.position.x < 3f)
        {
            //Right
            transform.position += new Vector3(2f, 0f, 0f);
        }
    }
}
