﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    Environment environment;

    [SerializeField] private float moveSpeed = 1f;
    [SerializeField] private float forceAmount = 1f;
    [SerializeField] private Transform ballHoldingPos;
    [SerializeField] private OpenTreasure openTreasure;

    bool movePlayer;

    Rigidbody ballRigidbody;
    Monkey monkeyChild;

    IEnumerator moveAndKickCoroutine;
    IEnumerator moveForwardCoroutine;

    public bool MovePlayer
    {
        set
        {
            movePlayer = value;
            monkeyChild.PlayRunAnimation();
        }
    }

    private void Start() {
        environment = FindObjectOfType<Environment>();
        monkeyChild = GetComponentInChildren<Monkey>();

        ballRigidbody = environment.Ball.GetComponent<Rigidbody>();
        moveAndKickCoroutine = MoveAndKick();

        moveForwardCoroutine = MoveForwardByTakingBall();
        environment.MainCamera.SetCamTransformAndParent(0, null);
    }

    private void Update() {
        if (movePlayer) {
            float zPos = transform.position.z;
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(transform.position.x, transform.position.y, 24f), Time.deltaTime * moveSpeed);
            if (zPos > 21f) {
                CheerUp();
                environment.TeamPlacements.DeactivateAllTeams();
                MovePlayer = false;
            }
        }
    }

    public void AcquireBall() {
        environment.Ball.transform.parent = ballHoldingPos;

        environment.Ball.transform.localPosition = Vector3.zero;
        environment.Ball.transform.rotation = Quaternion.Euler(0f, 90f, 0f);

        environment.Ball.ToggleIsBallKinematic(true);
        environment.Ball.ToggleBallsTrail(false);

        environment.TeamPlacements.GenerateTeam();
        StartCoroutine(moveForwardCoroutine);
    }


    private void KickBall() {
        Vector3 forceDirectionVector = ((environment.ArrowTipPos() + Vector3.up * 3) - transform.position);
        ballRigidbody.AddForce(forceDirectionVector * 2.0f, ForceMode.Impulse);
        Invoke("CheckIfPlayerHasCollectedKey", 2f);
    }

    void CheerUp() {
        monkeyChild.PlayCheer();
        environment.Ball.transform.parent = null;
        environment.Ball.ToggleBallsTrail(false);
        environment.Ball.ToggleIsBallKinematic(true);
        Invoke("ReplacePlayerToKickTheBall", 2f);
    }

    void ReplacePlayerToKickTheBall() {
        environment.ShowGoalPostAndKey();
        environment.uiManager.SetTextImageSpriteAndActivate(UI_Manager.TextSpriteShowCase.TouchDown);
        transform.position = new Vector3(0f, transform.position.y, 12.4f);
        Invoke("ReplaceBall", 0.5f);
    }

    void ReplaceBall() {
        environment.Ball.ToggleBallsTrail(true);
        environment.Ball.transform.localPosition = new Vector3(transform.position.x, environment.Ball.transform.position.y, transform.position.z + 1f);
    }

    public void KickTheBall() {
        //StartCoroutine(moveAndKickCoroutine);


        if (!PlayerPrefs.HasKey("PlayerGetMonkey")) {
            PlayerPrefs.SetInt("Player1GetMonkey", PlayerPrefs.GetInt("Player1GetMonkey") + 1);

            if (PlayerPrefs.GetInt("PlayerGetMonkey") == 5) {
                OpenTreasure ot = GameObject.FindObjectOfType<OpenTreasure>();
                ot.treasureCanvas.SetActive(true);
                PlayerPrefs.SetInt("Player1GetMonkey", 0);

            }
        }
        KickBall();
    }

    IEnumerator MoveForwardByTakingBall() {
        environment.MainCamera.SetCamTransformAndParent(1, null);
        environment.MainCamera.FollowPlayerNow();
        yield return new WaitForSeconds(1f);

        float zPos = transform.position.z;

        while (zPos < 21f) {
            zPos = transform.position.z;
            yield return new WaitForSeconds(Time.deltaTime);
        }

        transform.position = new Vector3(0f, transform.position.y, transform.position.z);
        environment.MainCamera.SetCamTransformAndParent(2, null);
        yield return new WaitForSeconds(1f);
        environment.Ball.ToggleIsBallKinematic(false);

        environment.uiManager.ButtonAction = UI_Manager.ButtonActions.TapToGoal;

        StopCoroutine(moveForwardCoroutine);
    }

    IEnumerator MoveAndKick() {
        /*
        yield return new WaitForSeconds(1f);
        for (int i = 0; i < 3; i++)
        {
            transform.position += new Vector3(0f, 0f, 0.2f);
            yield return new WaitForSeconds(0.1f);
        }
        yield return new WaitForSeconds(0.3f);
        */
        KickBall();
        yield return new WaitForSeconds(3f);
        StopCoroutine(moveAndKickCoroutine);
    }

    void CheckIfPlayerHasCollectedKey() {
        environment.uiManager.ButtonAction = UI_Manager.ButtonActions.RestartGame;
        
        if (GameManager.instance.isPlayerWin) {
            openTreasure.OpenTreasurePanel();
        }

        if (GameManager.instance.isPlayerWin) {
            APIManager.sInstance.SetInventory(1, EItems.dabalooney, 2, callback_ => InventoryCallBack());

            environment.uiManager.ToggleRestartGameButton(true);
        }
        if (!GameManager.instance.isPlayerWin) {
            environment.uiManager.ToggleRestartGameButton(true);
        }
    }
    public InventoryItem InventoryCallBack() {
        print("Added Inventory");
        return null;
    }
    private void OnTriggerEnter(Collider other) {
        if (other.GetComponent<PlayerTeamMember>() != null ||
            other.GetComponent<OponentTeamMember>() != null) {
            environment.MainCamera.SetCamTransformAndParent(1, null);
            CheckIfPlayerHasCollectedKey();
            movePlayer = false;
            monkeyChild.PlayDie();
            environment.uiManager.SetTextImageSpriteAndActivate(UI_Manager.TextSpriteShowCase.Tachled);
            GameManager.instance.IsPlayerDied = true;
        }

        if (other.GetComponent<OneInGame>() != null) {
            Destroy(other.gameObject);
        }
    }
}
