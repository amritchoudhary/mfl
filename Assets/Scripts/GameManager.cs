﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public static GameManager instance;

    public bool isPlayerWin;

    private bool isPlayerDied;

    public bool IsPlayerDied
    {
        get
        {
            return isPlayerDied;
        }

        set
        {
            isPlayerDied = value;
        }
    }

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        } else
        {
            Destroy(this);
        }
    }
}
