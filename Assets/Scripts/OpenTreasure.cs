﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OpenTreasure : MonoBehaviour
{

    public GameObject treasureCanvas;
    public Text treasureResultText;
    public Button useKeyButton;
    public GameObject keyImage;
    public Button keyUseOverButton;
    private int giftsRemaining = 0;
    private int rollResult;

    // Use this for initialization
    void Start() {

    }

    public void OpenTreasurePanel() {
        treasureCanvas.gameObject.SetActive(true);
    }

    //Treasure 
    public void UseKeyOnTreasure() {
        int rand = UnityEngine.Random.Range(1, 6);
        giftsRemaining = rand;
        rollResult = -giftsRemaining;
        keyImage.SetActive(false);
        treasureResultText.text = "You Got " + rand.ToString() + " Coins";
        treasureResultText.gameObject.SetActive(true);
        useKeyButton.interactable = false;
        StartCoroutine(StartGiveGift());
    }

    IEnumerator StartGiveGift() {
        yield return new WaitForSeconds(3);
        keyUseOverButton.gameObject.SetActive(true);
    }

    public void UseKeyOver() {
        treasureCanvas.SetActive(false);
    }

    public void GiveGift() {
    }

    public void TurnOffGiftMark() {

    }
}
