﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OponentTeamMember : MonoBehaviour {

    [SerializeField] private Transform raycastPos;

    public float speed = 1f;
    IEnumerator moveForwardCoroutine;
    IEnumerator checkIfAnotherPlayerAtFrontCoroutine;

    Monkey monkeyChild;
    TeamPlacements teamPlacement;

    private void Start()
    {
        monkeyChild = GetComponentInChildren<Monkey>();
        moveForwardCoroutine = MoveForward();
        checkIfAnotherPlayerAtFrontCoroutine = CheckIfAnotherPlayerIsAtFrontEnum();
        StartCoroutine(moveForwardCoroutine);
        StartCoroutine(checkIfAnotherPlayerAtFrontCoroutine);
        teamPlacement = GetComponentInParent<TeamPlacements>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<PlayerTeamMember>() != null)
        {
            this.gameObject.SetActive(false);
            teamPlacement.Environment.InstantiateOneNumPrefab(this.transform.position);
            other.gameObject.SetActive(false);
        }
    }

    IEnumerator CheckIfAnotherPlayerIsAtFrontEnum()
    {
        Ray ray = new Ray(raycastPos.position, -transform.forward);
        RaycastHit hit;
        float zPos = transform.position.z;
        OponentTeamMember opTeamMem = null;

        yield return new WaitForSeconds(1f);

        while (zPos > -15f)
        {
            Debug.DrawRay(raycastPos.position, -transform.forward, Color.red, 5f);

            if (Physics.Raycast(ray, out hit, 2f))
            {
                if(hit.collider.GetComponent<OponentTeamMember>() != null)
                {
                    opTeamMem = hit.collider.GetComponent<OponentTeamMember>();

                    if(opTeamMem.speed < this.speed)
                    {
                        this.speed = (opTeamMem.speed / 3f);
                    }
                }
            }
            yield return new WaitForSeconds(1f);
        }
    }

    IEnumerator MoveForward()
    {
        yield return new WaitForSeconds(1f);
        monkeyChild.PlayRunAnimation();

        float zPos = transform.position.z;
        speed = Random.Range(0.02f, 0.1f);

        while (zPos > -26f)
        {
            if (GameManager.instance.IsPlayerDied)
            {
                StopCoroutine(moveForwardCoroutine);
            }
            if (!GameManager.instance.IsPlayerDied)
            {
                transform.position -= new Vector3(0f, 0f, speed);
                zPos = transform.position.z;
            }
            yield return new WaitForSeconds(0.02f);
        }
        StopCoroutine(moveForwardCoroutine);
    }
}
