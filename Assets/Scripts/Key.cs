﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Key : MonoBehaviour
{

    [SerializeField] private float rotationSpeed = 1f;

    int rotationDir;

    // Use this for initialization
    void Start() {
        rotationDir = (Random.Range(0, 5) < 3) ? -1 : 1;
    }

    // Update is called once per frame
    void Update() {
        Rotate();
    }

    void Rotate() {
        transform.Rotate(new Vector3(0f, 0f, Time.deltaTime * rotationSpeed * rotationDir));
    }

    private void OnTriggerEnter(Collider other) {

        if (other.GetComponent<Ball>() != null) {
            GameManager.instance.isPlayerWin = true;
            Destroy(this.gameObject);
        }
    }
}
