﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTeamMember : MonoBehaviour {

    Monkey monkeyChild;

    void Start () {
        float waitTime = Random.Range(2f, 3f);
        monkeyChild = GetComponentInChildren<Monkey>();
        Invoke("PlayStandingAnimation", waitTime);
    }

    void PlayStandingAnimation()
    {
        monkeyChild.PlayStanding();
    }
}
