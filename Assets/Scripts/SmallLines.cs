﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallLines : MonoBehaviour {

    public GameObject smallLineTile;
    public float posOffset = 0.25f;
    public float posIncrementOffset = 0.25f;


    // Use this for initialization
    void Start () {
        for (int i = 0; i < 200; i++)
        {
            GameObject st = Instantiate(smallLineTile, smallLineTile.transform.position, smallLineTile.transform.rotation);
            st.transform.position = new Vector3(0f, 0.1f, smallLineTile.transform.position.z + posOffset);
            st.transform.parent = this.transform;
            posOffset += posIncrementOffset;
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
