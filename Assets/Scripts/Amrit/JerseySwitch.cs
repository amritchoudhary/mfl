﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JerseySwitch : MonoBehaviour {

	public Material playerMat;
	public Material enemyMat;
	public List<Texture> jerseryTextures;

	void Awake(){
		playerMat.mainTexture = jerseryTextures[APIManager.sInstance.team1Index];
		enemyMat.mainTexture = jerseryTextures[APIManager.sInstance.team2Index];
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
