﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Server : NetworkBehaviour
{
    public List<PlayerCoinToss> _players = new List<PlayerCoinToss>();
    public List<ETeam> _teams = new List<ETeam>();
    public List<ETossResult> _tossResults = new List<ETossResult>();

    // Use this for initialization
    void Start() {


    }

    // Update is called once per frame
    void Update() {

    }

    public int NewPlayer(PlayerCoinToss player_) {
        _players.Add(player_);
        _teams.Add(ETeam.None);
        _tossResults.Add(ETossResult.None);
        return _players.Count - 1;
    }

    public void ChooseTeam() {
        _players.ForEach(X => X.ChooseTeam());
    }

    public void ChooseTeamDone(int playerIndex_, ETeam team_) {
        _teams[playerIndex_] = team_;

        if (!(_teams.Exists(X => X == ETeam.None))) {
            ShowAnimation();
        }
    }

    public void ShowAnimation() {
        _players[0].ShowTeams((int)_teams[0], (int)_teams[1]);
        _players[1].ShowTeams((int)_teams[1], (int)_teams[0]);
    }

    public void CoinToss() {
        _players.ForEach(X => X.CoinToss());
    }

    public void CoinTossOver(int playerIndex_, ETossResult result_) {
        _tossResults[playerIndex_] = result_;

        if (!(_tossResults.Exists(X => X == ETossResult.None))) {
            FindResult();
        }
    }

    public void FindResult() {
        _players[0].GetResult((int)_tossResults[0], (int)_tossResults[1]);
        _players[1].GetResult((int)_tossResults[0], (int)_tossResults[1]);
    }

}
