﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class WinResult {
	public ETossResult result1;
	public ETossResult result2;
	public ETossResult winner;
	public Sprite resultSprite;
}
