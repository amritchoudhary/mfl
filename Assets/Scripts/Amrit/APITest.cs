﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class APITest : MonoBehaviour
{

    public Animator anim;

    // Use this for initialization
    void Start() {
        anim.Play("bomb");
    }

    public void Callback() {
        print(APIManager.sInstance.userID);
    }

    // Update is called once per frame
    void Update() {

    }

    private void DabalooneyCallback(Inventory inventory_) {
        print(inventory_);
    }
}
