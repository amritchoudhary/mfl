﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using System;

public class GUIManager : MonoBehaviour
{
    public static GUIManager sInstance;

    public GameObject _chooseTeamCanvas;
    public GameObject _showTeamsCanvas;
    public GameObject _coinTossCanvas;
    public GameObject _resultCanvas;
    private PlayerCoinToss mPlayer;
    private int selectedTeamIndex = 0;

    public Text _showTeamsText;
    public Text _resultText;
    public Image TeamSelection;
    public Animator TeamselectionAnim;
    public Image TeamSelection_1;
    public Animator TeamselectionAnim_1;

    [SerializeField] private Image resultImage;

    [SerializeField] private Image player_1_TeamImage;
    [SerializeField] private Image player_2_TeamImage;

    [SerializeField] private Sprite[] resultSprites;
    [SerializeField] private Sprite resultTieSprite;
    [SerializeField] private Sprite pl_1_WinSprite;
    [SerializeField] private Sprite pl_2_WinSprite;
    [SerializeField] private int[] resultSpriteCorrespondingMapping;

    public Sprite[] _teamLogos;

    public string pickCombination;
    public List<WinResult> winResults = new List<WinResult>();
    public Image resultSprite;
    public Image resultPlayerSprite;
    public Sprite emptySprite;
    private int mSelectedOnCoin = 0;

    public GameObject payDabalooneyPanel;
    public Button payDabalooneyButton;
    public Text dabalooneyText;
    private int availableDabalooney = 0;
    public GameObject showOptionsPanel;
    public Animator showOptionsAnimator;
    public GameObject optionButtonsPanel;
    public GameObject optionsReveal;
    public Animator optionsRevealAnimator;

    void Awake() {
        sInstance = this;
    }

    // Use this for initialization
    void Start() {
        if (PlayerPrefs.HasKey("TeamIndex1")) {
            ChooseTeamDone();
        }
    }

    // Update is called once per frame
    void Update() {
        if (Input.GetKeyDown(KeyCode.X)) {
            ETossResult r1 = (ETossResult)(UnityEngine.Random.Range(1, 6));
            ETossResult r2 = (ETossResult)(UnityEngine.Random.Range(1, 6));
            Debug.Log(r1 + ", " + r2);
            ChooseAndShowWinner(r1, r2);
        }
    }

    public void SelectAnyOneTeam(int teamIndex_) {
        selectedTeamIndex = teamIndex_;
    }

    public void SelectAmong_5_options(int selectedIndex_) // 5 = Monkey, 4 = Bomb, 3 = Rock, 2 = Scissor, 1 = Paper
    {
        pickCombination += selectedIndex_.ToString();
        int combinationValue = int.Parse(pickCombination);

        string reverseCombination = Reverse(pickCombination);
        int reverseCombinationValue = int.Parse(reverseCombination);

        if (combinationValue > 10) {
            if (combinationValue == 11 || combinationValue == 22 || combinationValue == 33 || combinationValue == 44 || combinationValue == 55) {
                resultImage.sprite = resultTieSprite;
                return;
            }
            int setSpriteOfIndex = 0;

            for (int i = 0; i < resultSpriteCorrespondingMapping.Length; i++) {
                if (combinationValue == resultSpriteCorrespondingMapping[i]) {
                    setSpriteOfIndex = i;
                    Invoke("ShowPlayer_1_Won", 3f);
                    break;
                } else if (reverseCombinationValue == resultSpriteCorrespondingMapping[i]) {
                    setSpriteOfIndex = i;
                    Invoke("ShowPlayer_2_Won", 3f);
                    break;
                }
            }
            resultImage.sprite = resultSprites[setSpriteOfIndex];
        }
    }
    public void SelectOnCoin(int selectedIndex_) {
        mSelectedOnCoin = selectedIndex_;
        Debug.Log(selectedIndex_);
    }

    public void ChooseTeam(PlayerCoinToss player_) {
        mPlayer = player_;
        _chooseTeamCanvas.SetActive(true);
    }

    public void SetPlayerOnGUIManager(PlayerCoinToss player_) {
        mPlayer = player_;
    }

    public void ChooseTeamDone() {
        _chooseTeamCanvas.SetActive(false);
        mPlayer.ChooseTeamDone((ETeam)selectedTeamIndex);
    }

    public void ShowTeams(int teamIndex1_, int teamIndex2_) {
        APIManager.sInstance.team1Index = teamIndex1_;
        APIManager.sInstance.team2Index = teamIndex2_;

        PayDabalooneyOpen();
    }
    public void ShowAnimation(string Animation_Name) {
        //TeamSelection.gameObject.SetActive(true);
        //TeamselectionAnim.enabled = true; 
        //TeamselectionAnim.Play(Animation_Name);
        //_showTeamsCanvas.SetActive(false);

    }
    void ObjectDisable() {
        print("Object Disbled");
        //TeamSelection.gameObject.SetActive(false);
        //TeamselectionAnim.enabled = false; 
        _showTeamsCanvas.SetActive(false);

    }

    public void CoinToss() {
        _showTeamsCanvas.SetActive(false);
        _coinTossCanvas.SetActive(true);
        mSelectedOnCoin = 0;
        Invoke("CoinTossStart", 8.5f);
    }

    public void CoinTossStart() {
        CoinTossOver();
    }

    public void CoinTossOver() {
        if (mSelectedOnCoin == 0) {
            mSelectedOnCoin = UnityEngine.Random.Range(1, 6);
        }
        mPlayer.CoinTossOver((ETossResult)mSelectedOnCoin);
        _coinTossCanvas.SetActive(false);
    }

    public void ShowResult(ETossResult player1, ETossResult player2) {
        _resultCanvas.SetActive(true);
        ChooseAndShowWinner(player1, player2);
    }

    public void StartGame() {
        SceneManager.LoadScene("GameScene");
    }

    private void ShowPlayer_1_Won() {
        resultImage.sprite = pl_1_WinSprite;
    }

    private void ShowPlayer_2_Won() {
        resultImage.sprite = pl_2_WinSprite;
    }

    public static string Reverse(string s) {
        char[] charArray = s.ToCharArray();
        Array.Reverse(charArray);
        return new string(charArray);
    }

    public void ChooseAndShowWinner(ETossResult player1, ETossResult player2) {

        if (player1 == player2) {
            resultPlayerSprite.sprite = resultTieSprite;
            resultSprite.sprite = emptySprite;
        } else {
            WinResult result = winResults.Find(X => ((X.result1 == player1 && X.result2 == player2) ||
                                                     (X.result1 == player2 && X.result2 == player1)));

            if (result.result1 == player1) {
                resultPlayerSprite.sprite = pl_1_WinSprite;
                //if(!PlayerPrefs.HasKey("Player1GetMonkey"))
                //{
                //    PlayerPrefs.SetInt("Player1GetMonkey", PlayerPrefs.GetInt("Player1GetMonkey") + 1);

                //    if(PlayerPrefs.GetInt("Player1GetMonkey")==)
                //}

            } else {
                resultPlayerSprite.sprite = pl_2_WinSprite;
            }
            resultSprite.sprite = result.resultSprite;
        }
    }

    public void PayDabalooneyOpen() {
        payDabalooneyPanel.SetActive(true);
        payDabalooneyButton.interactable = false;
        APIManager.sInstance.GetUserInventory(APIManager.sInstance.userID, DabalooneyCallback);
    }

    private void DabalooneyCallback(Inventory inventory_) {
        availableDabalooney = inventory_.dabalooney.amount;
        dabalooneyText.text = availableDabalooney.ToString();
        payDabalooneyButton.interactable = true;
    }

    public void PayDabalooney() {
        if (availableDabalooney > 0) {
            APIManager.sInstance.SetInventory(APIManager.sInstance.userID, EItems.dabalooney, availableDabalooney - 1, null);
            OpenShowOptions();
        }
    }

    public void OpenShowOptions() {
        payDabalooneyPanel.SetActive(false);
        showOptionsPanel.SetActive(true);

        showOptionsAnimator.enabled = true;
        //TeamselectionAnim.

        int teamIndex1_ = APIManager.sInstance.team1Index;

        string animation_name = "";
        if (teamIndex1_ == 1)
            animation_name = "ChicagoBerrys_Animation";
        else if (teamIndex1_ == 2)
            animation_name = "CincinnatiCougars_Animation";
        else if (teamIndex1_ == 3)
            animation_name = "ClevelandCoconuts_Animation";
        else if (teamIndex1_ == 4)
            animation_name = "DallasCowmonkeys_Animation";
        else if (teamIndex1_ == 5)
            animation_name = "Grrenbaygorillas_Animation";
        else if (teamIndex1_ == 6)
            animation_name = "JacksonvilleCheetahs_Animation";
        else if (teamIndex1_ == 7)
            animation_name = "KansasCityChimps_Animation";
        else if (teamIndex1_ == 8)
            animation_name = "LasVegasLlamacorns_Animation";
        else if (teamIndex1_ == 9)
            animation_name = "MiamiAquaMonkeys_Animation";
        else if (teamIndex1_ == 10)
            animation_name = "PittsburgSimians_Animation";
        else if (teamIndex1_ == 11)
            animation_name = "SandiegoSloths_Animation";
        else if (teamIndex1_ == 12)
            animation_name = "SanFranciscoGoldenrecievers_Animation";

        showOptionsAnimator.Play(animation_name);
        Invoke("ActivateOptionButtons", 2.5f);
    }

    public void ActivateOptionButtons() {
        optionButtonsPanel.SetActive(true);
    }

    public void SelectOption(int index) {    // 5 = Monkey, 4 = Bomb, 3 = Rock, 2 = Scissor, 1 = Paper
        showOptionsAnimator.gameObject.SetActive(false);
        optionButtonsPanel.SetActive(false);
        optionsReveal.SetActive(true);

        optionsRevealAnimator.enabled = true;

        string animation_name = "";

        if (index == 5)
            animation_name = "monkey";

        if (index == 4)
            animation_name = "bomb";

        if (index == 3)
            animation_name = "rock";

        if (index == 2)
            animation_name = "scissor";

        if (index == 1)
            animation_name = "paper";

        optionsRevealAnimator.Play(animation_name);

        mSelectedOnCoin = index;

        Invoke("StopShowingSelectedOption", 3.5f);
    }

    public void StopShowingSelectedOption() {
        showOptionsPanel.SetActive(false);
        CoinTossOver();
    }

}
