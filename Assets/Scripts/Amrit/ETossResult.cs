public enum ETossResult{
    None,
    Paper,
    Scissor,
    Bomb,
    Monkey,
    Rock,
}