﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerCoinToss : NetworkBehaviour
{

    private Server mServer;
    public int _playerIndex;

    public override void OnStartServer() {
        InitPlayer();
    }

    private void InitPlayer() {
        mServer = FindObjectOfType<Server>();
        _playerIndex = mServer.NewPlayer(this);
    }

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

    public void ChooseTeam() {
        RpcChooseTeam();
    }

    [ClientRpc]
    private void RpcChooseTeam() {
        if (isLocalPlayer) {
            //GUIManager.sInstance.ChooseTeam(this);
            GUIManager.sInstance.SetPlayerOnGUIManager(this);
            ETeam team = (ETeam)PlayerPrefs.GetInt("SelectedTeam", 1);
            ChooseTeamDone(team);
        }
    }

    public void ChooseTeamDone(ETeam team_) {
        CmdChooseTeamDone((int)team_);
    }

    [Command]
    private void CmdChooseTeamDone(int teamIndex_) {
        mServer.ChooseTeamDone(_playerIndex, (ETeam)teamIndex_);
    }

    //public void ShowTeams(int teamIndex1_, int teamIndex2_) 
    //{
    //    print("Team Index 1=" + teamIndex1_);
    //    print("Team Index 2= " + teamIndex2_);
    //    RpcShowTeams(teamIndex1_, teamIndex2_);

    //}

    //[ClientRpc]
    //private void RpcShowTeams(int teamIndex1_, int teamIndex2_) {
    //    if (isLocalPlayer) {
    //        GUIManager.sInstance.ShowTeams(teamIndex1_, teamIndex2_);
    //    }
    //}

    public void ShowTeams(int teamIndex1_, int teamIndex2_) {

        RpcShowTeams(teamIndex1_, teamIndex2_);

    }

    [ClientRpc]
    private void RpcShowTeams(int teamIndex1_, int teamIndex2_) {
        if (isLocalPlayer) {
            GUIManager.sInstance.ShowTeams(teamIndex1_, teamIndex2_);
        }
    }

    //public void ShowAnimation(string animation_name)
    //{
    //    RpcShowAnimation(animation_name);
    //}

    //[ClientRpc]
    //private void RpcShowAnimation(string animation_name)
    //{
    //    if (isLocalPlayer)
    //    {
    //        //GUIManager.sInstance.ShowTeams(teamIndex1_, teamIndex2_);
    //        GUIManager.sInstance.ShowAnimation(animation_name);
    //    }

    //}



    public void CoinToss() {
        RpcCoinToss();
    }

    [ClientRpc]
    private void RpcCoinToss() {
        if (isLocalPlayer) {
            GUIManager.sInstance.CoinToss();
        }
    }

    public void CoinTossOver(ETossResult result_) {
        CmdCoinTossOver((int)result_);
    }

    [Command]
    private void CmdCoinTossOver(int result_) {
        mServer.CoinTossOver(_playerIndex, (ETossResult)result_);
    }

    public void GetResult(int tossPlayer1_, int tossPlayer2_) {
        RpcGetResult(tossPlayer1_, tossPlayer2_);
    }

    [ClientRpc]
    private void RpcGetResult(int tossPlayer1_, int tossPlayer2_) {
        if (isLocalPlayer) {
            GUIManager.sInstance.ShowResult((ETossResult)tossPlayer1_, (ETossResult)tossPlayer2_);
        }
    }

}
