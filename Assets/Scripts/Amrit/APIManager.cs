﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class APIManager : MonoBehaviour
{

    public static APIManager sInstance;

    void Awake() {
        if (sInstance == null) {
            DontDestroyOnLoad(gameObject);
            sInstance = this;
        } else if (sInstance != this) {
            Destroy(gameObject);
        }
    }

    private WWWForm mForm;
    private WWW mDownload;
    private string mUrl = "https://api.3monkey.games/9J9TDT";
    [HideInInspector]
    public int userID;
    [HideInInspector]
    public bool isLogined = false;
    private UnityAction mLoginCallback_;
    private UnityAction<UserInfo> mUserInfoCallback_;
    private UnityAction<Inventory> mUserInventoryCallback_;
    private UnityAction<InventoryChange> mSetInventoryCallback_;

    public int team1Index = 1;
    public int team2Index = 1;

    public bool isAIMode = false;

    public void Login(string username_, string password_, UnityAction callback_) {
        mLoginCallback_ = callback_;
        mForm = new WWWForm();
        mForm.AddField("request", "login");
        mForm.AddField("user", username_);
        mForm.AddField("pass", password_);

        StartCoroutine("SubmitFormLogin", mForm);
    }

    IEnumerator SubmitFormLogin(WWWForm wwwForm_) {
        mDownload = new WWW(mUrl, wwwForm_);
        yield return mDownload;

        if (!string.IsNullOrEmpty(mDownload.error)) {
            print("Error downloading: " + mDownload.error);
        } else {
            ParseLogin(mDownload.text);
        }
    }

    private void ParseLogin(string loginString_) {
        string[] subStrings = loginString_.Split(':');
        int returnCode = int.Parse(subStrings[0]);
        string returnMessage = subStrings[1];

        if (returnCode == 1) {
            userID = int.Parse(subStrings[2]);
            isLogined = true;
        }

        mLoginCallback_();
    }

    public void LogOut() {
        userID = -1;
        isLogined = false;
    }

    public void GetUserInfo(int id_, UnityAction<UserInfo> callback_) {
        mUserInfoCallback_ = callback_;
        mForm = new WWWForm();
        mForm.AddField("request", "getUser");
        mForm.AddField("uid", id_);

        StartCoroutine("SubmitFormGetUserInfo", mForm);
    }

    IEnumerator SubmitFormGetUserInfo(WWWForm wwwForm_) {
        mDownload = new WWW(mUrl, wwwForm_);
        yield return mDownload;

        if (!string.IsNullOrEmpty(mDownload.error)) {
            print("Error downloading: " + mDownload.error);
        } else {
            ParseUserInfo(mDownload.text);
        }
    }

    private void ParseUserInfo(string userInfoString_) {
        UserInfo userInfo = JsonUtility.FromJson<UserInfo>(userInfoString_);
        mUserInfoCallback_(userInfo);
    }

    public void GetUserInventory(int id_, UnityAction<Inventory> callback_) {
        mUserInventoryCallback_ = callback_;
        mForm = new WWWForm();
        mForm.AddField("request", "getInv");
        mForm.AddField("uid", id_);

        StartCoroutine("SubmitFormGetUserInventory", mForm);
    }

    IEnumerator SubmitFormGetUserInventory(WWWForm wwwForm_) {
        mDownload = new WWW(mUrl, wwwForm_);
        yield return mDownload;

        if (!string.IsNullOrEmpty(mDownload.error)) {
            print("Error downloading: " + mDownload.error);
        } else {
            ParseUserInventory(mDownload.text);
        }
    }

    private void ParseUserInventory(string userInventoryString_) {
        Inventory inventory = JsonUtility.FromJson<Inventory>(userInventoryString_);
        mUserInventoryCallback_(inventory);
    }

    public void SetInventory(int id_, EItems item_, int change_, UnityAction<InventoryChange> callback_) {
        mSetInventoryCallback_ = callback_;
        mForm = new WWWForm();
        mForm.AddField("request", "setInv");
        mForm.AddField("uid", id_);
        mForm.AddField("itemid", (int)item_);
        mForm.AddField("change", change_);

        StartCoroutine("SubmitFormSetInventory", mForm);
    }

    IEnumerator SubmitFormSetInventory(WWWForm wwwForm_) {
        mDownload = new WWW(mUrl, wwwForm_);
        yield return mDownload;

        if (!string.IsNullOrEmpty(mDownload.error)) {
            print("Error downloading: " + mDownload.error);
        } else {
            ParseSetInventory(mDownload.text);
        }
    }

    private void ParseSetInventory(string setInventoryString_) {
        InventoryChange inventoryChange = JsonUtility.FromJson<InventoryChange>(setInventoryString_);
        if(mSetInventoryCallback_ != null)
            mSetInventoryCallback_(inventoryChange);
    }
}

public class UserInfo
{
    public int userid;
    public string username;
    public string type;
    public string fname;
    public string lname;
    public string email;
    public string bio;
    public string verified;
    public int xp;
    public string joined;
    public string lastseen;

    public override string ToString() {
        return JsonUtility.ToJson(this, true);
    }
}

[System.Serializable]
public class InventoryItem
{
    public string id;
    public int amount;
}

public class Inventory
{
    public InventoryItem dabalooney;
    public InventoryItem raffleTicket;
    public InventoryItem monkeyPoints;
    public InventoryItem bountyCoins;


    public override string ToString() {
        return JsonUtility.ToJson(this, true);
    }
}

public enum EItems
{
    dabalooney = 1,
    raffleTicket = 2,
    monkeyPoints = 3,
    bountyCoins = 4,
}

public class InventoryChange
{
    public int id;
    public string name;
    public int amount;

    public override string ToString() {
        return JsonUtility.ToJson(this, true);
    }
}
