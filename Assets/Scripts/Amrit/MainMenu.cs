﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class MainMenu : MonoBehaviour
{
    public InputField UserName;
    public InputField Password;
    public Text DisplayMessage;
    public Image CoinToss;
    public Image StartToss;
    public GameObject Panel;
    public Text Welcome_user;
    public GameObject loginPanel;
    public GameObject modeSelectPanel;
    public GameObject standingsPanel;
    public GameObject teamSelectPanel;
    private int mSelectedTeamIndex = 1;
    public GameObject payDabalooneyPanel;
    public Button payDabalooneyButton;
    public Text dabalooneyText;
    private int availableDabalooney;

    void Start() {

    }
    void Update() {

    }

    public void StartCoinToss() {
        SceneManager.LoadScene("CoinToss");
    }

    public void StartMatch() {
        SceneManager.LoadScene("GameScene");
    }

    public void LoginButton_Click() {
        if (UserName.text == "") {
            DisplayMessage.text = "Username can not be empty..";
        } else if (Password.text == "") {
            DisplayMessage.text = "Password can not be empty..";
        } else {
            DisplayMessage.text = "";
            APIManager.sInstance.Login(UserName.text, Password.text, LoginCallBack);
        }
    }

    public void LoginCallBack() {
        if (APIManager.sInstance.isLogined) {
            loginPanel.gameObject.SetActive(false);
            modeSelectPanel.gameObject.SetActive(true);
        }
    }

    public void Logout() {
        APIManager.sInstance.LogOut();
        modeSelectPanel.gameObject.SetActive(false);
        loginPanel.gameObject.SetActive(true);
    }

    public void StartVsFriend() {
        //PayDabalooneyOpen();
        SceneManager.LoadScene("CoinToss");
    }

    public void StartVsOthers() {
        //PayDabalooneyOpen();
        SceneManager.LoadScene("CoinToss");
    }

    public void StartVsRobomonkey() {
        //PayDabalooneyOpen();
        SceneManager.LoadScene("CoinToss");
    }

    public void ShowStandings() {
        modeSelectPanel.SetActive(false);
        standingsPanel.SetActive(true);
    }

    public void CloseStandings() {
        standingsPanel.SetActive(false);
        modeSelectPanel.SetActive(true);
    }

    public void ShowSelectTeam() {
        modeSelectPanel.SetActive(false);
        teamSelectPanel.SetActive(true);
    }

    public void CloseSelectTeam() {
        teamSelectPanel.SetActive(false);
        modeSelectPanel.SetActive(true);
    }

    public void PickOneTeam(int index) {
        mSelectedTeamIndex = index;
    }

    public void SelectPickedTeam() {
        PlayerPrefs.SetInt("SelectedTeam", mSelectedTeamIndex);
        CloseSelectTeam();
    }

    public void PayDabalooneyOpen() {
        payDabalooneyPanel.SetActive(true);
        payDabalooneyButton.interactable = false;
        APIManager.sInstance.GetUserInventory(APIManager.sInstance.userID, DabalooneyCallback);
    }

    private void DabalooneyCallback(Inventory inventory_) {
        availableDabalooney = inventory_.dabalooney.amount;
        dabalooneyText.text = availableDabalooney.ToString();
        payDabalooneyButton.interactable = true;
    }

    public void PayDabalooneyClose() {
        payDabalooneyPanel.SetActive(false);
    }

    public void PayDabalooney() {
        if (availableDabalooney > 0) {
            APIManager.sInstance.SetInventory(APIManager.sInstance.userID, EItems.dabalooney, availableDabalooney - 1, null);
            SceneManager.LoadScene("CoinToss");
        }
    }

}
