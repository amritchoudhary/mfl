﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class SelectionMenu : MonoBehaviour 
{
    public static int SelectedMyTeam = 0;
    public static int SelectedOpponentTeam = 0;
    public Text Display_text;
    public Dropdown MySelection,OpponenetSelection; 
 
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void MovetoGameScene()
    {
        if (SelectedMyTeam == SelectedOpponentTeam)
        {
            print("Change the selection. both can't have same team.");
            Display_text.text = "Change the selection. both can't have same team.";
        }
        else
        {
            Display_text.text = "";
            SceneManager.LoadScene("GameScene");
        }

    }
    public void ChangeIndex()
    {
        print("SelectedMyTeam=" +MySelection.value );
        print("SelectedOpponentTeam=" + OpponenetSelection.value);

        SelectedMyTeam = MySelection.value;
        SelectedOpponentTeam = OpponenetSelection.value;

    }
}
