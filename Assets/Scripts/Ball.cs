﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {


    public void ToggleIsBallKinematic(bool isKinematic_)
    {
        GetComponent<Rigidbody>().isKinematic = isKinematic_;
    }

    public void ToggleBallsTrail(bool isTrailEnabled_)
    {
        GetComponent<TrailRenderer>().enabled = isTrailEnabled_;
    }
}
