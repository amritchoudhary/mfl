﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour {

    [SerializeField] private bool swing;
    [SerializeField] private float frequencey = 0.5f;
    [SerializeField] private float amplitude = 1f;

    [SerializeField] private Transform arrowTip;

    private void Update()
    {
        SwingArrow();
    }

    void SwingArrow()
    {
        if(swing)
        {
            transform.position = new Vector3((Mathf.Sin(Time.time * frequencey) * amplitude), transform.position.y, transform.position.z);

           // transform.rotation = Quaternion.Euler(0f, (Mathf.Sin(Time.time * frequencey) * amplitude), 0f);
        }
    }

    public Vector3 ArrowTipPos()
    {
        return arrowTip.position;
    }
}
