﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlBallThrower : MonoBehaviour {
    [SerializeField] Environment    environment;
	[SerializeField] Player         player;
	[SerializeField] Ball           ball;
	[SerializeField] float          forceAmount = 1f;

    Monkey monkeyChild;

    Rigidbody ballRigidbody;

    IEnumerator moveAndKickCoroutine;

    private void Start()
    {
        ballRigidbody = ball.GetComponent<Rigidbody>();
		moveAndKickCoroutine = MoveAndKick();
        monkeyChild = GetComponentInChildren<Monkey>();
	}

   
    public void MoveAndKickBall()
    {
        StartCoroutine(moveAndKickCoroutine);
    }

    void KickBall(){
        Vector3 forceVector = new Vector3(Random.Range(-3f, 3f), 2f, -Random.Range(12f, 15f)) * forceAmount;

        ballRigidbody.AddForce(forceVector, ForceMode.Impulse);
    }

    IEnumerator MoveAndKick(){
        monkeyChild.PlayRunAnimation();
        yield return new WaitForSeconds(1f);
        for (int i = 0; i < 7; i++)
        {
            transform.position += new Vector3(0f, 0f, -0.2f);
            yield return new WaitForSeconds(0.1f);
        }
        monkeyChild.PlayKickAnimation();
        yield return new WaitForSeconds(0.8f);
		KickBall();
        yield return new WaitForSeconds(2f);
        monkeyChild.PlayIdle();
      //  GameManager.instance.InstantiateEnemies();
        yield return new WaitForSeconds(1f);
        player.AcquireBall();
        yield return new WaitForSeconds(0.5f);
        player.MovePlayer = true;

        this.gameObject.SetActive(false);
        StopCoroutine(moveAndKickCoroutine);
	}
}
