﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Environment : MonoBehaviour {

    public UI_Manager uiManager;

    [SerializeField] GameMainCamera mainCamera;
    [SerializeField] Player player;
    [SerializeField] PlBallThrower playerBallThrower;
    [SerializeField] Ball ball;
    [SerializeField] TeamPlacements teamPlacement;
    [SerializeField] Arrow arrow;
    [SerializeField] GoalPost goalPost;
    [SerializeField] Key key;

    [SerializeField] private GameObject oneNumberPrefab;

    public GameMainCamera   MainCamera { get { return mainCamera; } }
    public TeamPlacements   TeamPlacements { get { return teamPlacement; } }
    public Player           Player { get { return player; } }
    public PlBallThrower    PlayerBallThrower { get { return playerBallThrower; } }
    public Ball             Ball { get { return ball; } }

    public Vector3 ArrowTipPos() {
        return arrow.ArrowTipPos();
    }

    public void ShowGoalPostAndKey()
    {
        goalPost.gameObject.SetActive(true);
        key.gameObject.SetActive(true);
        arrow.gameObject.SetActive(true);
    }

    public void InstantiateOneNumPrefab(Vector3 position_)
    {
        Instantiate(oneNumberPrefab, new Vector3(position_.x, 0f, position_.z), Quaternion.identity);
    }
    
}
