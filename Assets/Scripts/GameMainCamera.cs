﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMainCamera : MonoBehaviour {

    [SerializeField] private Transform[] mainCamPosRotTransforms;

    bool followPlayer;
    Player player;

    Vector3 offset;

    private void Start()
    {
        player = FindObjectOfType<Player>();
    }

    private void Update()
    {
        if(followPlayer)
        {
            transform.position = new Vector3(0f, player.transform.position.y + offset.y, player.transform.position.z + offset.z);
        }
    }

    public void FollowPlayerNow()
    {
        offset = this.transform.position - player.transform.position;
        followPlayer = true;
    }

    public void SetCamTransformAndParent(int index_, Transform parent_)
    {
        CopyTransformsValues(mainCamPosRotTransforms[index_], this.transform);
        this.transform.parent = parent_;
    }

    void CopyTransformsValues(Transform fromTransform_, Transform toTransform_)
    {
        toTransform_.position = fromTransform_.position;
        toTransform_.rotation = fromTransform_.rotation;
    }
}
