﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlinkingText : MonoBehaviour {

    bool mIsScaledDown;

    private void Start()
    {
        InvokeRepeating("Blink", 0.5f, 0.5f);
    }

    void Blink()
    {
        if(mIsScaledDown)
        {
            transform.localScale = Vector3.zero;
        }
        if (!mIsScaledDown)
        {
            transform.localScale = Vector3.one;
        }
        mIsScaledDown = !mIsScaledDown;
    }
}
