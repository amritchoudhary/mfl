﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleUpStayThenDown : MonoBehaviour {

    [SerializeField] private float scaleSpeed = 2f;

    bool mIsScalingUp;
    bool mScaleNow = true;
    float mScale;

	void OnEnable () {
        transform.localScale = Vector3.zero;
        mIsScalingUp = false;
        mScaleNow = true;
	}
	
	void Update () {
        Scale();
	}

    void Scale()
    {
        if(!mIsScalingUp && mScaleNow)
        {
            if(mScale < 1f)
            {
                mScale += Time.deltaTime * scaleSpeed;
            }
            if(mScale > 1f)
            {
                Invoke("MakeScaleNowTrue", 1f);
                mIsScalingUp = true;
                mScaleNow = false;
            }
        }
        if (mIsScalingUp && mScaleNow)
        {
            if (mScale > 0f)
            {
                mScale -= Time.deltaTime * scaleSpeed;
            }
            if (mScale < 0.1f)
            {
                mIsScalingUp = false;
                mScaleNow = false;
                this.gameObject.SetActive(false);
            }
        }
        transform.localScale = new Vector3(mScale, mScale, mScale);
    }

    void MakeScaleNowTrue()
    {
        mScaleNow = true;
    }
}
