﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UI_Manager : MonoBehaviour 
{
    public Text TextForDabalooney;
    Environment environment;

    [SerializeField] GameObject tapToStartText;
    [SerializeField] Image textImage;
    [Header("-------------------------------------------")]
    [SerializeField] Sprite touchDownSprite;
    [SerializeField] Sprite itsGoodSprite;
    [SerializeField] Sprite tachledSprtie;
    [Header("-------------------------------------------")]
    [SerializeField] private Button fullScreenButton;
    [SerializeField] private Button restartGameButton;
    public ButtonActions buttonAction;
    private int rank;

    public ButtonActions ButtonAction
    {
        set
        {
            ToggleFullScreenButton(true);
            buttonAction = value;
        }
    }
    private void Start()
    {
        environment = FindObjectOfType<Environment>();
        ButtonAction = ButtonActions.TapToStart;
        tapToStartText.SetActive(true);

        APIManager.sInstance.SetInventory(1,EItems.dabalooney,-1,callback_=> InventoryCallBack());

        APIManager.sInstance.GetUserInventory(1, callback_ => GetUserInventoryCallback());
    }
    public void GetUserInventoryCallback()
    {
        print("Print Inventory");
    }
    public InventoryItem InventoryCallBack()
    {
        print("Added Inventory");
        return null;
    }
    public void ShowMessage()
    {
        fullScreenButton.gameObject.SetActive(true);
    }
    public void ToggleRestartGameButton(bool isActive_)
    {
        restartGameButton.gameObject.SetActive(isActive_);
    }
    public void RestartGame()
    {
        SceneManager.LoadScene(0);
    }
    public void SetTextImageSpriteAndActivate(TextSpriteShowCase textSpriteShowCase_)
    {
        switch (textSpriteShowCase_)
        {
            case TextSpriteShowCase.none:
                break;
            case TextSpriteShowCase.TouchDown:
                textImage.sprite = touchDownSprite;
                break;
            case TextSpriteShowCase.ItsGood:
                textImage.sprite = itsGoodSprite;
                break;
            case TextSpriteShowCase.Tachled:
                textImage.sprite = tachledSprtie;
                break;
            default:
                break;
        }
        textImage.gameObject.SetActive(true);
    }
    public void ToggleFullScreenButton(bool isActive_)
    {
        fullScreenButton.gameObject.SetActive(isActive_);
    }
    public void OnFullScreenButtonActionPressed()
    {
        switch (buttonAction)
        {
            case ButtonActions.none:
                break;
            case ButtonActions.TapToStart:
                tapToStartText.SetActive(false);
                environment.PlayerBallThrower.MoveAndKickBall();
                break;
            case ButtonActions.TapToGoal:
                environment.Player.KickTheBall();
                break;
            case ButtonActions.RestartGame:
                RestartGame();
                break;
            default:
                break;
        }
        ToggleFullScreenButton(false);
    }
    public enum ButtonActions
    {
        none,
        TapToStart,
        TapToGoal,
        RestartGame
    }
    public enum TextSpriteShowCase
    {
        none,
        TouchDown,
        ItsGood,
        Tachled
    }
}
