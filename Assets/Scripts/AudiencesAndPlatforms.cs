﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class AudiencesAndPlatforms : MonoBehaviour {

    [SerializeField] private Mesh[] prefabAudienceMeshes;
    [SerializeField] private GameObject[] audiencesInTheGame;

    List<Transform> group_1_Audiences = new List<Transform>();
    List<Transform> group_2_Audiences = new List<Transform>();
    List<Transform> group_3_Audiences = new List<Transform>();
    List<Transform> group_4_Audiences = new List<Transform>();

    IEnumerator audience_1_AnimateCoroutine;
    IEnumerator audience_2_AnimateCoroutine;
    IEnumerator audience_3_AnimateCoroutine;
    IEnumerator audience_4_AnimateCoroutine;

    void Start () {
        SetRandomAudienceMeshes();
        audience_1_AnimateCoroutine = AnimateAudienceGroup(group_1_Audiences);
        audience_2_AnimateCoroutine = AnimateAudienceGroup(group_2_Audiences);
        audience_3_AnimateCoroutine = AnimateAudienceGroup(group_3_Audiences);
        audience_4_AnimateCoroutine = AnimateAudienceGroup(group_4_Audiences);

        StartCoroutine(audience_1_AnimateCoroutine);
        StartCoroutine(audience_2_AnimateCoroutine);
        StartCoroutine(audience_3_AnimateCoroutine);
        StartCoroutine(audience_4_AnimateCoroutine);
    }

    void Update () {
		
	}


    void SetRandomAudienceMeshes()
    {
        for (int i = 0; i < audiencesInTheGame.Length; i++)
        {
            audiencesInTheGame[i].GetComponent<MeshFilter>().mesh = RandomMeshFromMeshPrefabs();

            int randomCond = UnityEngine. Random.Range(1, 6);

            switch (randomCond)
            {
                case 1:
                    group_1_Audiences.Add(audiencesInTheGame[i].transform);
                    break;

                case 2:
                    group_2_Audiences.Add(audiencesInTheGame[i].transform);
                    break;

                case 3:
                    group_3_Audiences.Add(audiencesInTheGame[i].transform);
                    break;

                case 4:
                    group_4_Audiences.Add(audiencesInTheGame[i].transform);
                    break;

                default:
                    break;
            }
        }
    }

    Mesh RandomMeshFromMeshPrefabs()
    {
        return prefabAudienceMeshes[UnityEngine.Random.Range(0, prefabAudienceMeshes.Length)];
    }

    IEnumerator AnimateAudienceGroup(List<Transform> groupAudiencesList_)
    {
        bool isAudienceUP = Convert.ToBoolean(UnityEngine.Random.Range(0, 2));
        float audienceJumpHight = UnityEngine.Random.Range(0.03f, 0.25f);
        float animationWaitTime = UnityEngine.Random.Range(0.2f, 0.4f);

        yield return new WaitForSeconds(1f);

        while (true)
        {
            for (int i = 0; i < groupAudiencesList_.Count; i++)
            {
                if (isAudienceUP)
                {
                    groupAudiencesList_[i].localPosition += new Vector3(0f, -audienceJumpHight, 0f);
                }
                if (!isAudienceUP)
                {
                    groupAudiencesList_[i].localPosition += new Vector3(0f, audienceJumpHight, 0f);
                }
            }

            isAudienceUP = !isAudienceUP;

            yield return new WaitForSeconds(animationWaitTime);
        }
    }

}
