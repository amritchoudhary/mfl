﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monkey : MonoBehaviour
{
    [SerializeField] private MeshFilter body;
    [SerializeField] private MeshFilter jersy;
    [SerializeField] private MeshFilter head;
    [SerializeField] private MeshFilter helmet;
    [SerializeField] private MeshFilter handLeft;
    [SerializeField] private MeshFilter handRight;
    [SerializeField] private MeshFilter legLeft;
    [SerializeField] private MeshFilter legRight;
    [SerializeField] private MeshFilter pantLeft;
    [SerializeField] private MeshFilter pantRight;
    [SerializeField] private MeshFilter shoeLeft;
    [SerializeField] private MeshFilter shoeRight;

    Animator animator;

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    public void PlayRunAnimation()
    {
        animator.SetTrigger("Run");
    }

    public void PlayKickAnimation()
    {
        animator.SetTrigger("Kick");
    }

    public void PlayIdle()
    {
        animator.SetTrigger("Idle");
    }

    public void PlayStanding()
    {
        animator.SetTrigger("Standing");
    }

    public void PlayDie()
    {
        animator.SetTrigger("Die");
    }

    public void PlayCheer()
    {
        animator.SetTrigger("Cheer");
    }

    public void ChangeMesh(MeshFilter meshFilter_, Mesh mesh_)
    {
        meshFilter_.mesh = mesh_;
    }
}
