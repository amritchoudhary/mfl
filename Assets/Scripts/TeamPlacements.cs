﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeamPlacements : MonoBehaviour {
    [SerializeField] private Teams[] teamsPosToBeSetOn;
    [SerializeField] private Teams[] teamsToBeSetTo;
    [SerializeField] private Environment environment;

    bool teamGenerated;

    public Environment Environment
    {
        get
        {
            return environment;
        }
    }

    public void GenerateTeam()
    {
        if (!teamGenerated)
        {
            int randomTeam = Random.Range(0, teamsPosToBeSetOn.Length);

            for (int i = 0; i < teamsPosToBeSetOn[randomTeam].opponentTeams.Length; i++)
            {
                teamsToBeSetTo[0].opponentTeams[i].position = teamsPosToBeSetOn[randomTeam].opponentTeams[i].position;
                teamsToBeSetTo[0].opponentTeams[i].gameObject.SetActive(true);
            }

            for (int i = 0; i < teamsPosToBeSetOn[randomTeam].playerTeams.Length; i++)
            {
                teamsToBeSetTo[0].playerTeams[i].position = teamsPosToBeSetOn[randomTeam].playerTeams[i].position;
                teamsToBeSetTo[0].playerTeams[i].gameObject.SetActive(true);
            }
            teamGenerated = true;
        }
    }
    
    public void DeactivateAllTeams()
    {
        for (int i = 0; i < teamsToBeSetTo[0].playerTeams.Length; i++)
        {
            teamsToBeSetTo[0].opponentTeams[i].gameObject.SetActive(false);
            teamsToBeSetTo[0].playerTeams[i].gameObject.SetActive(false);
        }
    }
}

[System.Serializable]
public class Teams
{
	public Transform[] opponentTeams;
	public Transform[] playerTeams;
}
